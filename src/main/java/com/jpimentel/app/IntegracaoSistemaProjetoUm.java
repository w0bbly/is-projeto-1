package com.jpimentel.app;

import java.io.*;

import static com.jpimentel.app.JsonNormal.*;

public class IntegracaoSistemaProjetoUm {

    public static void main(String[] args) throws IOException {

        //TEST SHORT
        encodeDecodeShortJsonStructure(1);

        //TEST LONG
        encodeDecodeLongJsonStructure(30);

        //JSON GZIP
        final var professors = JsonGzip.generateData(30, 36);
        JsonGzip.marshallGzip(professors);
        JsonGzip.unmarshallGzip();
    }
}
