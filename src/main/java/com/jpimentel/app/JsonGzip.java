package com.jpimentel.app;

import com.jpimentel.app.Json.Professor;
import com.jpimentel.app.Json.Student;
import org.apache.commons.lang.RandomStringUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static com.jpimentel.app.JsonNormal.sizeOf;

public class JsonGzip {

    public static List<Professor> generateData(int numberOfProfessors, int numberOfStudents) {

        List<Professor> professors = new ArrayList<>();

        for (int i = 0; i < numberOfProfessors; i++) {

            Professor professor = new Professor();

            professor.setId((long) i);
            professor.setName(RandomStringUtils.randomAlphabetic(10));
            professor.setAddress(RandomStringUtils.randomAlphabetic(20));
            professor.setPhone(RandomStringUtils.randomAlphabetic(9));
            professor.setDateOfBirth(LocalDate.now());

            for (int j = 0; j < numberOfStudents; j++) {

                Student student = new Student();

                student.setId((long) j);
                student.setName(RandomStringUtils.randomAlphabetic(10));
                student.setAddress(RandomStringUtils.randomAlphabetic(20));
                student.setPhone(RandomStringUtils.randomAlphabetic(9));
                student.setDateOfBirth(LocalDate.now());
                student.setRegistrationDate(LocalDate.now());
                student.setGender(RandomStringUtils.randomAlphabetic(4));

                if(professor.getStudents() != null)
                    professor.getStudents().add(student);
                else
                    professor.setStudents(List.of(student));
            }

            professors.add(professor);
        }

        return professors;
    }

    public static void marshallGzip(List<Professor> professors) {

        try {

            long preMarshall = System.currentTimeMillis();
            System.out.println("Starting marshalling ................................ " + preMarshall);
            System.out.println("Starting marshalling size ................................" + sizeOf(professors));

            FileOutputStream fileOutputStream = new FileOutputStream("E:\\Mestrado\\3 Ano\\Integracao de Sistemas\\src\\main\\resources\\json1.gz");
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(fileOutputStream);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(gzipOutputStream);

            objectOutputStream.writeObject(professors);

            objectOutputStream.close();

            long postMarshall = System.currentTimeMillis();
            System.out.println("Finished marshalling ................................ " + postMarshall);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void unmarshallGzip() {

        List<Professor> professors;

        try {

            long preMarshall = System.currentTimeMillis();
            System.out.println("Starting unmarshalling ................................ " + preMarshall);

            FileInputStream fileInputStream = new FileInputStream("E:\\Mestrado\\3 Ano\\Integracao de Sistemas\\src\\main\\resources\\json1.gz");
            GZIPInputStream gzipInputStream = new GZIPInputStream(fileInputStream);
            ObjectInputStream objectInputStream = new ObjectInputStream(gzipInputStream);

            professors = (List<Professor>) objectInputStream.readObject();
            objectInputStream.close();

            long postMarshall = System.currentTimeMillis();
            System.out.println("Finished unmarshalling ................................ " + postMarshall);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
