// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: professor_student.proto

package com.jpimentel.app.ProtoBufferModels;

public final class ProfessorStudentProtos {
  private ProfessorStudentProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_jpimentel_app_Student_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_jpimentel_app_Student_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_jpimentel_app_Professor_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_jpimentel_app_Professor_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_jpimentel_app_ProfessorStudent_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_jpimentel_app_ProfessorStudent_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\027professor_student.proto\022\021com.jpimentel" +
      ".app\"\310\001\n\007Student\022\n\n\002id\030\001 \001(\003\022\014\n\004name\030\002 \001" +
      "(\t\022\r\n\005phone\030\003 \001(\t\0221\n\006gender\030\004 \001(\0162!.com." +
      "jpimentel.app.Student.Gender\022\025\n\rdate_of_" +
      "birth\030\005 \001(\t\022\031\n\021registration_date\030\006 \001(\t\022\017" +
      "\n\007address\030\007 \001(\t\"\036\n\006Gender\022\n\n\006FEMALE\020\000\022\010\n" +
      "\004MALE\020\001\"\212\001\n\tProfessor\022\n\n\002id\030\001 \001(\003\022\014\n\004nam" +
      "e\030\002 \001(\t\022\r\n\005phone\030\003 \001(\t\022\025\n\rdate_of_birth\030" +
      "\004 \001(\t\022\017\n\007address\030\005 \001(\t\022,\n\010students\030\006 \003(\013" +
      "2\032.com.jpimentel.app.Student\"C\n\020Professo" +
      "rStudent\022/\n\tprofessor\030\001 \003(\0132\034.com.jpimen" +
      "tel.app.ProfessorB?\n#com.jpimentel.app.P" +
      "rotoBufferModelsB\026ProfessorStudentProtos" +
      "P\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_jpimentel_app_Student_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_jpimentel_app_Student_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_jpimentel_app_Student_descriptor,
        new java.lang.String[] { "Id", "Name", "Phone", "Gender", "DateOfBirth", "RegistrationDate", "Address", });
    internal_static_com_jpimentel_app_Professor_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_com_jpimentel_app_Professor_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_jpimentel_app_Professor_descriptor,
        new java.lang.String[] { "Id", "Name", "Phone", "DateOfBirth", "Address", "Students", });
    internal_static_com_jpimentel_app_ProfessorStudent_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_com_jpimentel_app_ProfessorStudent_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_jpimentel_app_ProfessorStudent_descriptor,
        new java.lang.String[] { "Professor", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
