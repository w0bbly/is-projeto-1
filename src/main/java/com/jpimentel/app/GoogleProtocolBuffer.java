package com.jpimentel.app;


import com.jpimentel.app.ProtoBufferModels.Professor;
import com.jpimentel.app.ProtoBufferModels.ProfessorStudent;
import com.jpimentel.app.ProtoBufferModels.Student;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;


public class GoogleProtocolBuffer {

    private static final Integer PROFESSORS_NUMBER = 30;
    private static final Integer STUDENTS_NUMBER = 36;
    private static final String LOCAL_TIME = LocalDateTime.now().toString();
    private static final String FILENAME = "protobufferfile-" + LOCAL_TIME;
    private static final Boolean PRINT_FILE = false;


    public static void main(String[] args) throws IOException {

        ProfessorStudent.Builder wrapper = ProfessorStudent.newBuilder();

        // Read the existing file.
        try {
            wrapper.mergeFrom(new FileInputStream(FILENAME));
        } catch (FileNotFoundException e) {
            System.out.println(FILENAME  + ": File not found.  Creating a new file.");
        }

        // Add Professors
        List<Professor> professors = addProfessors();

        for(Professor professor : professors){
            wrapper.addProfessor(professor);
        }

        // Write the new file back to disk.
        long preEncode = System.currentTimeMillis();
        System.out.println("GPB Pre encoding ................................ " + preEncode + " milliseconds");

        FileOutputStream output = new FileOutputStream(FILENAME, true);
        wrapper.build().writeTo(output);

        long postEncode = System.currentTimeMillis();
        System.out.println("GPB Post encoding ................................ " + postEncode + " milliseconds");
        System.out.println("Finished encoding size ................................ " + output.getChannel().size()
                + " bytes");

        output.close();

        readFromFile();
    }

    static List<Professor> addProfessors(){
        List<Professor> professorsList = new ArrayList<>();

        for(int i = 0; i < PROFESSORS_NUMBER; i++){
            Professor.Builder professor = Professor.newBuilder();
            professor.setId(i);
            professor.setName(RandomStringUtils.randomAlphabetic(10));
            professor.setAddress(RandomStringUtils.randomAlphabetic(10));
            professor.setDateOfBirth(RandomStringUtils.randomNumeric(4));

            addStudents(professor);

            professorsList.add(professor.build());
        }
        return professorsList;
    }

    static void addStudents(Professor.Builder professor) {

        for(int i = 0; i < STUDENTS_NUMBER; i++){
            Student.Builder student = Student.newBuilder();
            student.setId(Long.parseLong(RandomStringUtils.randomNumeric(3)));
            student.setName(RandomStringUtils.randomAlphabetic(10));
            student.setGender(Student.Gender.forNumber(RandomUtils.nextInt(1)));
            student.setAddress(RandomStringUtils.randomAlphabetic(10));
            student.setDateOfBirth(RandomStringUtils.randomNumeric(4));
            student.setPhone(RandomStringUtils.randomNumeric(9));
            student.setRegistrationDate(RandomStringUtils.randomNumeric(4));
            professor.addStudents(student.build());
        }
    }

    static void readFromFile() throws IOException {
        // Read the existing file.
        long preDecode = System.currentTimeMillis();
        System.out.println("GPB Pre decoding ................................ " + preDecode + " milliseconds");

        ProfessorStudent professorStudent = ProfessorStudent.parseFrom(new FileInputStream(FILENAME));

        long postDecode = System.currentTimeMillis();
        System.out.println("GPB Post decoding ................................ " + postDecode + " milliseconds");

        if(PRINT_FILE) {
            for (Professor professor : professorStudent.getProfessorList()) {
                System.out.println("Professor ID: " + professor.getId());
                System.out.println("  Professor Name: " + professor.getName());

                for (Student student : professor.getStudentsList()) {
                    System.out.println("  Student ID: " + student.getId());
                    System.out.println("    Name: " + student.getName());
                    System.out.println("    Phone: " + student.getPhone());
                    System.out.println("    Gender: " + student.getGender().name());
                    System.out.println("    Date of Birth: " + student.getDateOfBirth());
                    System.out.println("    Registration Date: " + student.getRegistrationDate());
                    System.out.println("    Address: " + student.getAddress());
                }
            }
        }

    }
}

